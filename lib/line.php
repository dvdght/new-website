<?php
	
	function line_top($txt)
	{
		echo '<table>';
		echo '<tr>';
		echo '<td class="line_color"></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td>' . $txt . '</td>';
		echo '</tr>';
		echo '</table>';
	}
	
	function line_bottom($txt)
	{
		echo '<table>';
		echo '<tr>';
		echo '<td>' . $txt . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td class="line_color"></td>';
		echo '</tr>';
		echo '</table>';
	}
	
	function line_left($txt)
	{
		echo '<table>';
		echo '<tr>';
		echo '<td class="line_color"></td>';
		echo '<td>' . $txt . '</td>';
		echo '</tr>';
		echo '</table>';
	}
	
	function line_right($txt)
	{
		echo '<table>';
		echo '<tr>';
		echo '<td>' . $txt . '</td>';
		echo '<td class="line_color"></td>';
		echo '</tr>';
		echo '</table>';
	}
	
?>
