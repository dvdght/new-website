<?php
	
	function form_begin()
	{
		echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '">';
	}
	
	function form_end()
	{
		echo '</form>';
	}
	
	function form_input_text($name, $value = "", $size = 30)
	{
		echo '<input type="text" name="' . $name . '" value="' . $value . '" size="' . $size . '">';
	}
	
	function form_input_password($name, $size = 30)
	{
		echo '<input type="password" name="' . $name . '" size="' . $size . '">';
	}
	
	function form_submit($value)
	{
		echo '<input type="submit" value="' . $value . '">';
	}
	
?>
