<small>

Icons used are the <?php img_url('img/16x16/oxygen.png', 'http://techbase.kde.org/Projects/Oxygen', 'Oxygen icons'); ?> (<?php img_url('img/16x16/creative_commons.png', 'http://creativecommons.org/licenses/by-sa/3.0/', 'CC BY-SA 3.0', 'Creative Common Attribution-ShareAlike 3.0 License'); ?> and <?php img_url('img/16x16/gnu.png', 'http://www.gnu.org/licenses/lgpl.html', 'GNU LGPL', 'GNU Lesser General Public License'); ?>)
<br>

For logos, see the <?php img_url('img/16x16/gnu.png', './index.php?page=logos', 'logos licenses'); ?>
<br>

Powered by <i>new-website</i> <?php img_url('img/16x16/gitlab.png', 'https://gitlab.com/hnc/new-website', 'Gitlab'); ?> | <?php img_url('img/16x16/konqueror.png', 'http://new-website.toile-libre.org/', 'Demo website'); ?>

</small>
