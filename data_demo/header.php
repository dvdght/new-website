<div class="float_left">
	
	Welcome on <i>new-website</i> demo <?php img_url('img/16x16/gitlab.png', 'https://gitlab.com/hnc/new-website', 'Gitlab'); ?> | <?php img_url('img/16x16/konqueror.png', 'http://new-website.toile-libre.org/', 'Demo website'); ?>
	<br>
	
	<i>new-website</i> is a kind of simplified <?php url('http://en.wikipedia.org/wiki/Content_management_system', 'CMS'); ?>
	<br>
	
	<i>new-website</i> is a free software available under <?php url('http://www.gnu.org/licenses/agpl-3.0.html', 'GNU Affero General Public License 3+'); ?>
	<br>
	
</div>

<div class="float_right">
	<?php img(data_or_demo() . '/header.png'); ?>
</div>

<div class="clear"></div>
