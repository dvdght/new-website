<?php require_once("lib/lib.php"); ?>

<article>

	<p>This is an example of a contact form.</p> 

	<?php
		$a = rand(0, 50);
		$b = rand(0, 50);
		$c = $a + $b;

	    if ($_SERVER['REQUEST_METHOD']=='POST') 
	    {
		    $errors = 0;

		    if (!isset($_POST['your_email'])) 
		    {
			    $errors++;
			    $error1 = '<p>There is a problem with the variable "email".</p>';
		    } 
		    else 
		    {
			    if (empty($_POST['your_email'])) 
			    {
				    $errors++;
				    $error2 = '<p>Forgot to give your email.</p>';
			    } 
			    else 
			    {
				    if (!is_mail_address($_POST['your_email']) || !is_mail_address($_POST['his_email'])) 
				    {
					    $errors++;
					    $error3 = '<p>This email does not look like an email.</p>';
				    }
			    }
		    }
		     
		    if (!isset($_POST['your_message'])) 
		    {
			    $errors++;
			    $error4 = '<p>There is a problem with the variable "message".</p>';
		    } 
		    else 
		    {
			    if (empty($_POST['your_message'])) 
			    {
				    $errors++;
				    $error5 = '<p>You forgot to give a message.</p>';
			    }
		    }
		    if (!isset($_POST['captcha'])) 
		    {
			    $errors++;
			    $error6 = '<p>There is a problem with the variable "captcha".</p>';
		    } 
		    else 
		    {
			    if ($_POST['captcha']!=$_POST['resultat']+"/") 
			    {
				    $errors++;
				    $error7 = '<p>Sorry, the anti-spam captcha is incorrect.</p>';
				}
		    }
		    if ($errors==0) 
		    { 
		    	if(send_mail_html("Test", htmlentities($_POST['your_message']), htmlentities($_POST['his_email']), htmlentities($_POST['your_name']), htmlentities($_POST['your_email'])   )    )
		    	{
		    		echo '<h2>Your message was sent!</h2>';
		    	}
		    	else
		    	{
		    		$errors++;
		    		$error8 = '<p>ERROR</p>';
		    	}
	   		} 
		    else 
		    {
			    echo '<div style="border:1px solid #ff0000; padding:5px;">';
			    echo '<p style="color:#ff0000;">Sorry, there has been '.$errors.' error (s) . Here are the details of the errors:</p>';
			    if (isset($error1)) echo '<p>'.$error1.'</p>';
			    if (isset($error2)) echo '<p>'.$error2.'</p>';
			    if (isset($error3)) echo '<p>'.$error3.'</p>';
			    if (isset($error4)) echo '<p>'.$error4.'</p>';
			    if (isset($error5)) echo '<p>'.$error5.'</p>';
			    if (isset($error6)) echo '<p>'.$error6.'</p>';
			    if (isset($error7)) echo '<p>'.$error7.'</p>';
			    if (isset($error8)) echo '<p>'.$error8.'</p>';
			    echo '</div>';
		    }
    	}
	?>

	<form method="post" action="<?php echo strip_tags($_SERVER['REQUEST_URI']); ?>">
		<p>Name: <input type="text" name="your_name" size="30" /></p>
		<p>Email *: <input type="text" name="your_email" size="30" /></p>
		<p>Receiver Email* : <input type="text" name="his_email" size="30" /></p>
		<p>Message *:</p>
		<textarea name="your_message" cols="40" rows="10" id="your_message"></textarea>
		
		 <input type="hidden" name="resultat" <?php echo "value=".$c.""; ?>/>

		<p><?php echo $a; ?> + <?php echo $b; ?> = <input type="text" name="captcha" size="2" /></p>
		<p><input type="submit" name="submit" value="Send" /></p>
	</form>
	
</article>
